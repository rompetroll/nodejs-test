var http = require('http')
var fs = require('fs')
var Mustache = require('mustache')
var questions = require("./questions")

http.createServer(function (req, res) {
    var questionNumber = 1;
    var question = questions.getQuestion(questionNumber)
console.log("question: "+question)
    fs.readFile("./templates/index.html","UTF-8", function(err, template) {
        res.writeHeader(200, {'Content-Type': 'text/html'})
        res.end(Mustache.to_html(template, { q: question}))
    })
}).listen(8000);
