var q = [
    {   question: "Hvor gammel er du?", 
        answers: [
            {text: "5", correct: false},
            {text: "15", correct: true},
            {text: "25", correct: false}
        ]
    },
    {   question: "Hvor er det du bor?", 
        answers: [
            {text: "Oslo", correct: true},
            {text: "Bergen", correct: false}  
        ]
    }
]

exports.getQuestion = function(number) {
  return q[number]
}

